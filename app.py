"""App. Lets do something with models."""

import datetime

from models import *


def populate_database():
    """Add records to database."""

    ProviderDAO.delete()
    CanteenDAO.delete()

    provider1 = ProviderDAO.add(name='Rahva Toit')
    provider2 = ProviderDAO.add(name='Baltic Restaurants Estonia AS')
    provider3 = ProviderDAO.add(name='TTÜ Sport OÜ')
    provider4 = ProviderDAO.add(name='Bitstop kohvik OÜ')
    ProviderDAO.commit()

    canteen_mappings = [
        dict(name='Economics- and social science building canteen', location='Akadeemia tee 3 SOC- building',
             provider_id=provider1.id, time_open=datetime.time(8, 30), time_closed=datetime.time(18, 30)),
        dict(name='Library canteen', location='Akadeemia tee 1/Ehitajate tee 7',
             provider_id=provider1.id, time_open=datetime.time(8, 30), time_closed=datetime.time(19, 0)),
        dict(name='Main building Deli cafe', location='Ehitajate tee 5 U01 building',
             provider_id=provider2.id, time_open=datetime.time(9, 0), time_closed=datetime.time(16, 30)),
        dict(name='Main building Daily lunch restaurant', location='Ehitajate tee 5 U01 building',
             provider_id=provider2.id, time_open=datetime.time(9, 0), time_closed=datetime.time(16, 30)),
        dict(name='U06 building canteen', location='U06 building',
             provider_id=provider1.id, time_open=datetime.time(9, 0), time_closed=datetime.time(16, 0)),
        dict(name='Natural Science building canteen', location='Akadeemia tee 15 SCI building',
             provider_id=provider2.id, time_open=datetime.time(9, 0), time_closed=datetime.time(16, 0)),
        dict(name='ICT building canteen', location='Raja 15/Mäepealse 1',
             provider_id=provider2.id, time_open=datetime.time(9, 0), time_closed=datetime.time(16, 0)),
        dict(name='Sports building canteen', location='Männiliiva 7 S01 building',
             provider_id=provider3.id, time_open=datetime.time(11, 0), time_closed=datetime.time(20, 0))
    ]
    CanteenDAO.add_batch(canteen_mappings)

    # Here' s the ITCollege canteen
    CanteenDAO.add(provider_id=provider4.id,
                   name='bitStop KOHVIK',
                   location='Raja 4C ITCollege building',
                   time_open=datetime.time(9, 30),
                   time_closed=datetime.time(16, 0))
    CanteenDAO.commit()


if __name__ == '__main__':
    initialize_app('sqlite:///diners.db')
    populate_database()

    # show all providers
    print('\nProviders:')
    providers = ProviderDAO.all()
    for provider in providers:
        print('\t', provider)

    # create query for canteens which are serviced by 'Rahva Toit'
    print('\nCanteens which are serviced by "Rahva Toit":')
    canteens = ProviderDAO.first(Provider.name == 'Rahva Toit').canteens
    for canteen in canteens:
        print('\t', canteen)

    # create query for canteens which are open 16.15-18.00
    print('\nCanteens which are open 16.15-18.00:')
    canteens = CanteenDAO.all(Canteen.time_open <= datetime.time(16, 15), Canteen.time_closed >= datetime.time(18, 00))
    for canteen in canteens:
        print('\t', canteen)
