"""
Models.

Following documentation is an alternative way of creating this SQLite database by using Alembic script.
In the end everything can be done with SQLAlchemy ORM.
Keeping this alembic part for future reference.


alembic => alembic-script.py

1. Initialize 'alembic' directory:
   alembic init alembic

2. Set database url in alembic.ini:
   sqlalchemy.url = sqlite:///diners.db

3. Edit env.py:
   import sys
   import os
   sys.path.insert(0, os.getcwd())  # or PYTHONPATH=. and then alembic command?
   import models
   target_metadata = models.Base.metadata

4. Create new migration script:
   alembic revision --autogenerate -m "revision_name"

5. Apply migration:
   alembic upgrade head
"""

import sqlalchemy as sa
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker


Base = declarative_base()  # Base model, is defined by ORM


class Provider(Base):
    """Provider."""

    __tablename__ = 'PROVIDER'

    id = sa.Column('ID', sa.Integer, primary_key=True)
    name = sa.Column('Name', sa.String(64))

    canteens = relationship('Canteen', back_populates='provider')

    def __repr__(self):
        """Representation format."""
        return f'id: {self.id:3}, name: {self.name:35}, ' \
               f'canteens: {", ".join([_canteen.name for _canteen in self.canteens])}'


class Canteen(Base):
    """Canteen."""

    __tablename__ = 'CANTEEN'

    id = sa.Column('ID', sa.Integer, primary_key=True)
    provider_id = sa.Column('ProviderID', sa.Integer, sa.ForeignKey('PROVIDER.ID'), nullable=False)
    name = sa.Column('Name', sa.String(64))
    location = sa.Column('Location', sa.String(64))
    time_open = sa.Column('TimeOpen', sa.Time)
    time_closed = sa.Column('TimeClosed', sa.Time)

    provider = relationship('Provider', back_populates='canteens')

    def __repr__(self):
        """Representation format."""
        return f'id: {self.id:3}, provider: {self.provider.name:35}, name: {self.name:50}, ' \
               f'location: {self.location:35}, open: {self.time_open}-{self.time_closed}'


class BaseDAO:
    """Base data access object."""

    model = None
    session = None

    @classmethod
    def all(cls, *criterion):
        """
        Query all records or filtered records by optional criterion.

        Criterion structure:
            <model>.<attribute1> == <value1>, <model>.<attribute2> == <value2>, ...

        Criterion example:
            Person.name == 'Bob', Person.age >= 18

            By default comma stands for AND operation. For OR operation:
                from sqlalchemy import or_
                or_(Person.name == 'Bob', Person.age >= 18)

            canteens = CanteenDAO.all(or_(Canteen.location.like('%building%'), Canteen.location.ilike('%ehitajate%')))
            canteens = CanteenDAO.all(Canteen.location.like('%building%'), Canteen.location.ilike('%ehitajate%'))
        """
        if not criterion:
            return cls.session.query(cls.model).all()
        return cls.session.query(cls.model).filter(*criterion).all()

    @classmethod
    def first(cls, *criterion):
        """Query one record by optional criterion."""
        if not criterion:
            return cls.session.query(cls.model).first()
        return cls.session.query(cls.model).filter(*criterion).first()

    @classmethod
    def add_instance(cls, instance):
        """Add record by instance."""
        cls.session.add(instance)
        return instance

    @classmethod
    def add(cls, **kwargs):
        """Add record by attributes."""
        instance = cls.model(**kwargs)
        return cls.add_instance(instance)

    @classmethod
    def add_batch(cls, batch):
        """Add multiple records, mapped as dicts in iterable container."""
        cls.session.bulk_insert_mappings(cls.model, batch)

    @classmethod
    def delete_instance(cls, instance):
        """Delete single record."""
        cls.session.delete(instance)

    @classmethod
    def delete(cls, *criterion):
        """Delete all records or filtered records by optional criterion."""
        if not criterion:
            cls.session.query(cls.model).delete()
            return
        cls.session.query(cls.model).filter(*criterion).delete()

    @classmethod
    def count(cls):
        """Get the count of records."""
        return cls.session.query(cls.model).count()

    @classmethod
    def commit(cls):
        """Commit changes to database."""
        cls.session.commit()


class ProviderDAO(BaseDAO):
    """Provider data access object."""
    model = Provider


class CanteenDAO(BaseDAO):
    """Canteen data access object."""
    model = Canteen


def initialize_app(database_url: str) -> None:
    """Perform ORM-related initialization tasks to make our provided DAOs usable for the app."""
    engine = create_engine(database_url)

    Base.metadata.create_all(engine)
    Base.metadata.bind = engine

    BaseDAO.session = sessionmaker(bind=engine)()
