This homework uses SQLAlchemy package.

#### Files:
* __models.py__ - contains all model classes _(Provider, Canteen)_ and corresponding DAO classes _(BaseDAO, ProviderDAO, CanteenDAO)_. 
Also defines _initialize_app_ function that takes database url string as parameter. 
DAO methods are named as self-explanatory.
* __app.py__ - uses _models.py_ to perform some sample operations on SQLite database. Uses standard output.
* __diner.db__ - Included/generated SQLite database file for _canteens and providers_.
* __readme.md__ - This file.


Homework requested queries are inside \_\_main\_\_ part of __app.py__.
